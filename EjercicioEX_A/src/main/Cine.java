package main;

import java.util.ArrayList;
import java.util.Scanner;

public class Cine {
	public static Scanner sc = new Scanner(System.in);

	private String nombre;
	private ArrayList<Pelicula> peliculas;

	public Cine() {
		this.nombre = "";
		this.peliculas = new ArrayList<Pelicula>();
	}

	public Cine(String nombre, ArrayList<Pelicula> peliculas) {
		this.nombre = nombre;
		this.peliculas = peliculas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Pelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ArrayList<Pelicula> peliculas) {
		this.peliculas = peliculas;
	}

	public void visualizarCine() {
		int contador = 1;
		int contadorProt = 0;
		System.out.println();
		System.out.println("Nombre del cine: " + this.nombre);
		for (Pelicula p : this.peliculas) {
			System.out.println("Pelicula n�mero " + contador);
			System.out.println("--------------------");
			System.out.println("T�tulo: " + p.getTitulo());
			System.out.println("Precio P.V.P: " + p.getPvp());
			System.out.println("Listado de actores:");
			for (Actor a : p.getActores()) {
				System.out.println("Nombre: " + a.getNombre());
				System.out.println("�Es protagonista? " + a.isProtagonista());
				if (a.isProtagonista() == true) {
					contadorProt += 1;
				}
			}
			if (contadorProt > 2) {
				System.out.println("Genero familiar");
			} else {
				System.out.println("Genero rom�ntico");
			}
		}
	}

	public void generarUnCine() {
		String seleccion;
		String nombreC;
		String tituloP;
		
		System.out.println("Por favor introduzca el nombre del cine");
		nombreC = sc.nextLine();
		ArrayList<Pelicula> peliculas = new ArrayList<Pelicula>();
		while (true) {
			ArrayList<Actor> actores = new ArrayList<Actor>();
			
			Pelicula pelicula = new Pelicula();
			pelicula.rellenarPelicula();
			peliculas.add(pelicula);
			
			System.out.println("�Desea seguir haciendo peliculas?");
			seleccion = sc.nextLine();
			if (seleccion.equalsIgnoreCase("no")) {
				break;
			}
		}
		this.setNombre(nombreC);
		this.setPeliculas(peliculas);
	}

	public void modificarPelicula() {
		String titulo;
		String nAct;
		boolean encontrado = false;
		System.out.println("Por favor introduzca el nombre de la pelicula que desea buscar");
		titulo = sc.nextLine();
		System.out.println("Por favor introduzca el nombre del actor que desea modificar");
		nAct = sc.nextLine();
		for (Pelicula p : this.getPeliculas()) {
			if (p.getTitulo().equals(titulo)) {
				for (Actor a : p.getActores()) {
					if (nAct.equalsIgnoreCase(a.getNombre())) {
						System.out.println("Por favor introduzca el nuevo nombre del actor");
						a.setNombre(sc.nextLine());
						System.out.println("Por favor introduzca de nuevo si es protagonista o no");
						a.setProtagonista(sc.nextBoolean());
						encontrado = true;
						break;
					}
				}
				break;
			}
		}
		if(encontrado == false) {
			System.out.println("El actor no ha sido encontrado, por favor introduzca un titulo y un nombre v�lidos");
		}
	}

	public void OrdenarDescPeliculas() {
		ArrayList<Pelicula> temporal = this.getPeliculas();
		Pelicula temp = new Pelicula();
		for (int i = 0; i < temporal.size() - 1; i++) {
			for (int j = 0; j < temporal.size() - 1 - i; j++) {
				if (temporal.get(j).getTitulo().compareTo(temporal.get(j + 1).getTitulo()) < 0) {
					temp = temporal.get(j);
					temporal.set(j, temporal.get(j + 1));
					temporal.set(j + 1, temp);
				}
			}
		}
	}

}
