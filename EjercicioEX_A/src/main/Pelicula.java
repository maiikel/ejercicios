package main;
import java.util.ArrayList;
import java.util.Scanner;
public class Pelicula {
	private String titulo;
	private double pvp;
	private ArrayList<Actor> actores;
	
	public static Scanner sc = new Scanner(System.in);
	
	public Pelicula() {
		this.titulo = titulo;
		this.pvp = pvp;
		this.actores = new ArrayList<Actor>();
	}
	
	public Pelicula(String titulo, double pvp, ArrayList<Actor> actores) {
		this.titulo = titulo;
		this.pvp = pvp;
		this.actores = actores;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPvp() {
		return pvp;
	}

	public void setPvp(double pvp) {
		this.pvp = pvp;
	}

	public ArrayList<Actor> getActores() {
		return actores;
	}

	public void setActores(ArrayList<Actor> actores) {
		this.actores = actores;
	}
	
	public void rellenarPelicula() {
		int precioPVP = -1;
		String nombreA;
		System.out.println("Por favor introduzca el t�tulo de la pel�cula");
		this.setTitulo(sc.nextLine());
		System.out.println("Por favor introduzca el precio P.V.P de la pelicula");
		while (precioPVP < 0) {
			try {
				precioPVP = sc.nextInt();
				this.setPvp(precioPVP);
				sc.nextLine();
			} catch (Exception e) {
				System.out.println("Se ha producido un error al introducir el P.V.P");
				precioPVP = sc.nextInt();
				this.setPvp(precioPVP);
				sc.nextLine();
			}
		}
		
		while (true) {
			Actor actor = new Actor();
			System.out.println("Por favor introduzca el nombre del actor");
			nombreA = sc.nextLine();
			actor.setNombre(nombreA);
			if (actor.getNombre().equalsIgnoreCase("*")) {
				break;
			}
			System.out.println("Por favor introduzca si es protagonista o no (true � false)");
			actor.setProtagonista(sc.nextBoolean());
			sc.nextLine();
			actores.add(actor);
		}
	}
	
}
