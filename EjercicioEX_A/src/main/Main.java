package main;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		//Generar cine
		Cine cine1 = new Cine();
		//Cine cine2 = new Cine();
		
		//Rellenar cine
		cine1.generarUnCine();
		//cine2.generarUnCine();

		
		//Visualizar cine (Metodo visualizar en la clase cine)
		cine1.visualizarCine();
		//cine2.visualizarCine();

		
		//Modificar pelicula por titulo
		System.out.println("Modificar pelicula");
		System.out.println();
		cine1.modificarPelicula();
		//cine2.modificarPelicula();
		
		
		//Modificar titulo de pelicula
		cine1.OrdenarDescPeliculas();
		//cine2.OrdenarDescPeliculas();
		
		//Visualizar 
		System.out.println("Visualización después de ordenar y modificar");
		System.out.println();
		cine1.visualizarCine();
		//cine2.visualizarCine();
	}
}
