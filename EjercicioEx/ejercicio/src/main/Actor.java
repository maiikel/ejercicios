package main;

import java.util.Scanner;

public class Actor {
	private String nombre;
	private boolean esProtagonista;
	
	Actor(){
		this.nombre="";
		this.esProtagonista=false;
	}
	Actor(String nombre){
		this.nombre=nombre;
		this.esProtagonista=false;
	}
	Actor(String nombre, boolean esProtagonista){
		this.nombre=nombre;
		this.esProtagonista=esProtagonista;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isEsProtagonista() {
		return esProtagonista;
	}
	public void setEsProtagonista(boolean esProtagonista) {
		this.esProtagonista = esProtagonista;
	}
	
	public void rellenarActor() {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca el nombre del actor: ");
		this.nombre=in.nextLine();
		char prota;
		do {
			System.out.println("�Es el protagonista? (S/N)");
			prota=in.next().charAt(0);
			if (prota=='S' || prota == 's') {
				this.esProtagonista=true;
			}
			if (prota=='N' || prota == 'n') {
				this.esProtagonista=false;
			}
		}while(prota != 'N' && prota != 'n' && prota != 'S' && prota != 's');		
	}
	
	public void visualizarActor() {
		System.out.println("El nombre del actor es : " + this.nombre);
		System.out.print("�Es protagonista? : ");
		if (this.esProtagonista)
			System.out.println("S�.");
		else
			System.out.println("NO.");
	}
	
}
