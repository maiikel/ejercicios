package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Cine cine1 = new Cine();
		//Cine cine2 = new Cine();
		cine1.rellenarCine();
		cine1.visualizarCine();
		cine1.ordenarPeliculas();
		Scanner in = new Scanner (System.in);
		System.out.println("�Qu� pel�cula deseas buscar?");
		String peliculaBuscar = in.nextLine();
		cine1.buscarPelicula(peliculaBuscar);
	}

}
