package main;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Pelicula {
	private String titulo;
	private int pvp;
	private ArrayList<Actor> actores;
	
	Pelicula(){
		this.titulo="";
		this.pvp=0;
		actores = new ArrayList<Actor>();
	}
	Pelicula(String titulo, int pvp){
		this.titulo=titulo;
		this.pvp=pvp;
		actores = new ArrayList<Actor>();
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getPvp() {
		return pvp;
	}
	public void setPvp(int pvp) {
		this.pvp = pvp;
	}
	public ArrayList<Actor> getActores() {
		return actores;
	}
	public void setActores(ArrayList<Actor> actores) {
		this.actores = actores;
	}
	public void rellenarPelicula() {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca el t�tulo: ");
		this.titulo = in.nextLine();
		boolean errorLectura=true;
		do {
			try {
				System.out.println("Introduzca el precio: ");
				this.pvp = in.nextInt();
				errorLectura=false;
			}catch (InputMismatchException e) {
				System.out.println("Error de formato");
				in.nextLine();
				errorLectura=true;
			}catch (Exception e) {
				System.out.println("Error del lectura");
				errorLectura=true;
			}
		}while(errorLectura);
		System.out.println("�Cu�ntas actores quieres rellenar?");
		int numPeliculas=in.nextInt();
		for (int i=0; i<numPeliculas; i++) {
			Actor unActor= new Actor();
			unActor.rellenarActor();
			actores.add(unActor);			
		}
	}
	public void visualizarPelicula() {
		System.out.println("El t�tulo es: " + this.titulo);
		System.out.println("Su precio es : " + this.pvp);
		for (Actor actor : actores) {
			actor.visualizarActor();
		}
		
	}
	public void buscarActor(String actorBuscar) {
		boolean encontrado=false;
		for (Actor actor : actores) {
			if (actor.getNombre().compareTo(actorBuscar)==0) {
				System.out.println("Encontrado el actor " + actorBuscar);
				encontrado=true;
				Actor nuevoActor=new Actor();
				nuevoActor.rellenarActor();				
				actores.set(actores.indexOf(actor),nuevoActor);
			}
		}
		if(!encontrado)
			System.out.println("No encontrado el actor " +actorBuscar);
	}
}
