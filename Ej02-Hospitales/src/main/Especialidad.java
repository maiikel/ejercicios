package main;

import java.util.ArrayList;
import java.util.Scanner;

import javax.print.CancelablePrintJob;

public class Especialidad {
	private String nombreEspecialidad; 
	private ArrayList<Medico> medicos; 
	
	Especialidad(){
		this.nombreEspecialidad="";
		this.medicos= new ArrayList<Medico>();
			
	}
	Especialidad(String nombre){
		this.nombreEspecialidad=nombre;
		this.medicos= new ArrayList<Medico>();
			
	}
	
	void rellenarEspecialidad() {
		Scanner in = new Scanner(System.in);
		System.out.print("Nombre Especialidad =");
		this.nombreEspecialidad= in.nextLine();
		System.out.println("M�dicos del la especialidad " + this.nombreEspecialidad);
		System.out.println("�cuandos m�dicos hay?");
		int cantidadMedicos=in.nextInt();
		for (int i=1; i<=cantidadMedicos;i++) {
			//1.Creo un objeto
			Medico unMedico=new Medico();
			//2. Relleno un objeto
			unMedico.rellenarMedico();
			//3. Visualizo un objeto
			medicos.add(unMedico);
		}
		
	}
	void visualizarEspecialidad() {
		System.out.println("Especialidad = "+ this.nombreEspecialidad);
		for (Medico medico : medicos) {
			medico.visualizarMedico();
		}
	}
	void OrdenarMedicos() {
		Medico copiaMedico=new Medico();
		for (int j=1; j<this.medicos.size(); j++) {
			for (int i=this.medicos.size()-1; i>=j; i--) {
				if (this.medicos.get(i).getNombreMedico().compareTo(this.medicos.get(i-1).getNombreMedico())<0){
					 //intercambiar
					 copiaMedico=this.medicos.get(i);
					 this.medicos.set(i, medicos.get(i-1));
					 this.medicos.set(i-1, copiaMedico);
				}			
			}
		}
	}
	
	//---- getters, setters y tostring
	public String getNombreEspecialidad() {
		return nombreEspecialidad;
	}
	public void setNombreEspecialidad(String nombreEspecialidad) {
		this.nombreEspecialidad = nombreEspecialidad;
	}
	public ArrayList<Medico> getMedicos() {
		return medicos;
	}
	public void setMedicos(ArrayList<Medico> medicos) {
		this.medicos = medicos;
	}
	
	
	
}
