package main;

import java.util.Scanner;

public class Medico {
	private String nombreMedico; 
	private String apellidoMedico;
	
	//---- sobrecarga en constructores
	Medico(){
		this.nombreMedico="";
		this.apellidoMedico="";
	}
	Medico(String nombre, String apellido){
		this.nombreMedico=nombre;
		this.apellidoMedico=apellido;
	}
	
	//---- m�todos
	void rellenarMedico(){
		Scanner in = new Scanner(System.in);
		System.out.print("\tNombre =");
		this.nombreMedico=in.nextLine();
		System.out.print("\tApellido =" );
		this.apellidoMedico=in.nextLine();
	}
	void visualizarMedico(){
		System.out.println("\tNombre =" + this.nombreMedico);
		System.out.println("\tApellido =" + this.apellidoMedico);
		
	}
	//---- getters, setters y toString
	public String toString() {
		return "Medico [nombreMedico=" + nombreMedico + ", apellidoMedico=" + apellidoMedico + "]";
	}
	public String getNombreMedico() {
		return nombreMedico;
	}
	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}
	public String getApellidoMedico() {
		return apellidoMedico;
	}
	public void setApellidoMedico(String apellidoMedico) {
		this.apellidoMedico = apellidoMedico;
	}
	
	
}
