package main;

import java.util.ArrayList;
import java.util.Scanner;

public class Hospital {
	private String nombre;
	private String direccion;
	private ArrayList<Especialidad> especialidades;
	
	Hospital(){
		this.nombre="";
		this.direccion="";
		this.especialidades=new ArrayList<Especialidad>();
	}

	public void rellenarHospital() {
		Scanner in = new Scanner(System.in);
		System.out.println("\n*** INFORMACION DEL HOSPITAL ****\n");
		System.out.print("Nombre = \t");
		this.nombre=in.nextLine();
		System.out.print("Direccion = \t");
		this.direccion=in.nextLine();
		System.out.println("Especialidades del "+ this.nombre);
		System.out.print("¿cuantas especialidades hay?");
		int cantidadEspecialidades= in.nextInt();
		for(int i=0; i<cantidadEspecialidades; i++) {
			//1. creo un objeto
			Especialidad unaEspecialidad= new Especialidad();
			//2. relleno el objeto
			unaEspecialidad.rellenarEspecialidad();
			//3. lo meto al vector
			especialidades.add(unaEspecialidad);
		}
		
		
	}
	public void visualizarHospital() {
		System.out.println("\n*** INFORMACION DEL HOSPITAL ****\n");
		System.out.println("Nombre = \t"+ this.nombre);
		System.out.println("Direccion = \t"+ this.direccion);
		System.out.println("Especialidades del "+ this.nombre);
		for (Especialidad especialidad : especialidades) {
			especialidad.visualizarEspecialidad();
		}

	}
	public void ordenarHospital() {
		Especialidad copiaEspecialidad=new Especialidad();
		for (int j=1; j<this.especialidades.size(); j++) {
			for (int i=this.especialidades.size()-1; i>=j; i--) {
				if (this.especialidades.get(i).getNombreEspecialidad().compareTo(this.especialidades.get(i-1).getNombreEspecialidad())<0){
					 //intercambiar
					 copiaEspecialidad=this.especialidades.get(i);
					 this.especialidades.set(i, especialidades.get(i-1));
					 this.especialidades.set(i-1, copiaEspecialidad);
				}			
			}
		}
		for (Especialidad especialidad : especialidades) {
			especialidad.OrdenarMedicos();
		}
	}
}